---
startdate:  2019-01-25
starttime: "19:00"
enddate: 2019-02-02
#endtime: "05h"
allday: true
linktitle: "ByteWeek 2019"
title: "Byteweek 2019"
location: "HSBXL"
eventtype: "Eight bitdays of awesomeness"
price: ""
image: "byteweek.png"
series: "byteweek2019"
start: "true"
aliases: [/byteweek] 
---

The week before Fosdem conference, HSBXL compiles ByteWeek.  
Eight 'Bitdays' of hackatons, workshops and talks.  
We end our week with the notorious [Bytenight](/bytenight)!

# Friday January 25th
{{< events series="byteweek2019" when="2019-01-25" >}}
# Saturday January 26th
{{< events series="byteweek2019" when="2019-01-26" >}}
# Sunday January 27th
{{< events series="byteweek2019" when="2019-01-27" >}}
# Monday January 28th
{{< events series="byteweek2019" when="2019-01-28" >}}
# Tuesday January 29th
{{< events series="byteweek2019" when="2019-01-29" >}}
# Wednesday January 30th
{{< events series="byteweek2019" when="2019-01-30" >}}
# Thursday January 31th
{{< events series="byteweek2019" when="2019-01-31" >}}
# Friday February 1st
{{< events series="byteweek2019" when="2019-02-01" >}}
# Saturday February 2nd
{{< events series="byteweek2019" when="2019-02-02" >}}
# Call for participation
If you want to have a talk, workshop or hackaton,  
send a mail to **contact@hsbxl.be** with your proposal.

# Food
We are serving fresh soup every day.
