---
startdate:  2019-02-01
starttime: "18:00"
enddate:  2019-02-01
linktitle: "Pre-FOSDEM warmup with Kubernetes"
title: "Pre-FOSDEM warmup with Kubernetes"
location: "HSBXL"
eventtype: "Talk"
price: "Free"
series: "byteweek2019"
image: "kubernetes.png"
--- 

# About
Let’s kick off the Brussels Kubernetes Meetup with some awesome talks from experts in the field.

# Registration
Meetup link to follow...

# Food
Food sponsored by ...

